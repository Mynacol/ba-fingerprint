'use strict';

/* helper functions */
const ua = new UAParser()

// From https://stackoverflow.com/a/17923727
function functionName(func) {
    // Match:
    // - ^          the beginning of the string
    // - function   the word 'function'
    // - \s+        at least some white space
    // - ([\w\$]+)  capture one or more valid JavaScript identifier characters
    // - \s*        optionally followed by white space (in theory there won't be any here,
    //              so if performance is an issue this can be omitted[1]
    // - \(         followed by an opening brace
    //
    var result = /^function\s+([\w\$]+)\s*\(/.exec(func.toString())

    return result ? result[1] : '' // for an anonymous function there won't be a match
}

// From https://stackoverflow.com/a/7616484
function hash(string) {
    let hash = 0, i, chr;
    for (i = 0; i < string.length; i++) {
        chr = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash
}


/* canvas-related functions */
function probePath(path) {
    let l = []
    for (x = 0; x < canvas.width; x++) {
        for (y = 0; y < canvas.height; y++) {
            l[x][y] = context.isPointInPath(path, x, y, 'evenodd')
        }
    }
    return l
}

function probeStroke(path) {
    let l = []
    for (x = 0; x < canvas.width; x++) {
        for (y = 0; y < canvas.height; y++) {
            l[x][y] = context.isPointInStroke(path, x, y)
        }
    }
    return l
}


/* fingerprinting functions */
// From https://github.com/fingerprintjs/fingerprintjs/blob/master/src/sources/canvas.ts#L34
function fpjs3(context) {
    // Detect browser support of canvas winding
    // https://web.archive.org/web/20170825024655/http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
    // https://github.com/Modernizr/Modernizr/blob/master/feature-detects/canvas/winding.js
    context.rect(0, 0, 10, 10)
    context.rect(2, 2, 6, 6)
    const winding = !context.isPointInPath(5, 5, 'evenodd')

    context.textBaseline = 'alphabetic'
    context.fillStyle = '#f60'
    context.fillRect(125, 1, 62, 20)
    context.fillStyle = '#069'
    // This can affect FP generation when applying different CSS on different websites:
    // https://github.com/fingerprintjs/fingerprintjs/issues/66
    context.font = '11pt no-real-font-123'
    // The choice of emojis has a gigantic impact on rendering performance (especially in FF).
    // Some newer emojis cause it to slow down 50-200 times.
    // A bare emoji shouldn't be used because the canvas will change depending on the script encoding:
    // https://github.com/fingerprintjs/fingerprintjs/issues/66
    // Escape sequence shouldn't be used too because Terser will turn it into a bare unicode.
    const printedText = `Cwm fjordbank ${String.fromCharCode(55357, 56835)} gly`
    context.fillText(printedText, 2, 15)
    context.fillStyle = 'rgba(102, 204, 0, 0.2)'
    context.font = '18pt Arial'
    context.fillText(printedText, 4, 45)

    // Canvas blending
    // https://web.archive.org/web/20170826194121/http://blogs.adobe.com/webplatform/2013/01/28/blending-features-in-canvas/
    // http://jsfiddle.net/NDYV8/16/
    context.globalCompositeOperation = 'multiply'
    for (const [color, x, y] of [
        ['#f0f', 50, 50],
        ['#0ff', 100, 50],
        ['#ff0', 75, 100],
    ]) {
        context.fillStyle = color
        context.beginPath()
        context.arc(x, y, 50, 0, Math.PI * 2, true)
        context.closePath()
        context.fill()
    }

    // Canvas winding
    // http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
    // http://jsfiddle.net/NDYV8/19/
    context.fillStyle = '#f0f'
    context.arc(75, 75, 75, 0, Math.PI * 2, true)
    context.arc(75, 75, 25, 0, Math.PI * 2, true)
    context.fill('evenodd')
}

// fingerprintjs without text / font testing
function fpjs3_no_text(context) {
    //floatingPointMods(context)
    // Detect browser support of canvas winding
    // https://web.archive.org/web/20170825024655/http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
    // https://github.com/Modernizr/Modernizr/blob/master/feature-detects/canvas/winding.js
    context.rect(0, 0, 10, 10)
    context.rect(2, 2, 6, 6)
    const winding = !context.isPointInPath(5, 5, 'evenodd')

    context.textBaseline = 'alphabetic'
    context.fillStyle = '#f60'
    context.fillRect(125, 1, 62, 20)

    // Canvas blending
    // https://web.archive.org/web/20170826194121/http://blogs.adobe.com/webplatform/2013/01/28/blending-features-in-canvas/
    // http://jsfiddle.net/NDYV8/16/
    context.globalCompositeOperation = 'multiply'
    for (const [color, x, y] of [
        ['#f0f', 50, 50],
        ['#0ff', 100, 50],
        ['#ff0', 75, 100],
    ]) {
        context.fillStyle = color
        context.beginPath()
        context.arc(x, y, 50, 0, Math.PI * 2, true)
        context.closePath()
        context.fill()
    }

    // Canvas winding
    // http://blogs.adobe.com/webplatform/2013/01/30/winding-rules-in-canvas/
    // http://jsfiddle.net/NDYV8/19/
    context.fillStyle = '#f0f'
    context.arc(75, 75, 75, 0, Math.PI * 2, true)
    context.arc(75, 75, 25, 0, Math.PI * 2, true)
    context.fill('evenodd')
}

// simple bezier curve
function bezier(context) {
    context.moveTo(50, 50)
    context.beginPath()
    if (floatingPointTesting) {
        context.bezierCurveTo(11, 11, 21, 44, 40, 30)
    } else {
        context.bezierCurveTo(10, 10, 30, 30, 50, 50) //diagonal line
    }
    context.closePath()
    context.stroke()
}

// simple rectangle
function rect(context) {
    context.rect(40, 20, 60, 90)
    context.stroke()
}

// rectangle by drawing 4 lines
function rect_4lines(context) {
    context.beginPath()
    context.moveTo(40, 20)
    context.lineTo(100, 20)
    context.lineTo(100, 110)
    context.lineTo(40, 110)
    //context.closePath()
    context.stroke()
}

// unambiguous line
function straightLine(context) {
    context.lineWidth = 1 - 1/256
    context.moveTo(50, 50)
    context.lineTo(50, 100)
    context.stroke()
}

//line similar to bezier curve above
function diagonalLine(context) {
    //context.lineWidth = 1 + 39/256
    context.moveTo(50, 50)
    context.lineTo(10, 10)
    context.stroke()
}

// almost horizontal line
function px1line(context) {
    context.moveTo(20, 40)
    context.lineTo(160, 41)
    context.stroke()
}

// two lines intersecting
function intersect(context) {
    //context.lineWidth = 1 - 1/256
    context.moveTo(30, 100)
    context.lineTo(150, 100)
    context.stroke()

    context.moveTo(100, 30)
    context.lineTo(100, 150)
    context.stroke()
}

// font rendering
function font(context) {
    context.font = '18pt Arial'
    context.fillText('The quick brown fox ', 10, 50)
    context.fillText('jumps over the lazy dog', 10, 100)
}

// emoji
function emoji(context) {
    context.font = '72pt Arial'
    context.fillText('\ud83d\ude03', 10, 100)
}

// complex bezier curves
function bezier_complex(context) {
    //context.lineWidth = 1 + 1/512
    context.beginPath()
    context.moveTo(30, 20)
    context.bezierCurveTo(170, 10, 160, 60, 240, 50)
    context.stroke()

    context.beginPath()
    context.moveTo(20, 120)
    context.quadraticCurveTo(130, 80, 150, 100)
    context.stroke()
}

// arc directly and with bezier curves
function arc(context) {
    context.beginPath()
    context.arc(50, 50, 40, 0, 0.5 * Math.PI)
    context.lineTo(50, 50)
    context.closePath()
    context.fill()

    context.beginPath()
    context.moveTo(90, 100)
    const c1 = { x: 90 - 100 * 0.2652164898395440092, y: 100 + 90 * 0.2652164898395440092 }
    const c2 = { x: 50 + 140 * 0.2652164898395440092, y: 140 - 90 * 0.2652164898395440092 }
    context.bezierCurveTo(c1.x, c1.y, c2.x, c2.y, 50, 140)
    context.lineTo(50, 100)
    context.closePath()
    context.fill()

    context.beginPath()
    context.moveTo(140, 100)
    context.quadraticCurveTo(140, 140, 100, 140)
    context.lineTo(100, 100)
    context.closePath()
    context.fill()
}

function triangle(c2) {
    // from mozilla staff:
    // https://docs.google.com/document/d/1S0WbqJ576cHv03z1jSuUVaG6JqHDYuqMJ71XcU63sQI/edit
    c2.fillStyle = "blue";
    c2.beginPath();
    c2.moveTo(50, 50); 
    c2.lineTo(200, 200);
    c2.lineTo(175, 100);
    c2.closePath();
    c2.fill();
    c2.strokeStyle = "red";
    c2.lineWidth = 5;
    c2.stroke();
}

function rectCompare(context) {
    //context.lineCap = 'butt'
    //context.fillStyle = 'red'
    context.fillRect(10, 10, 80, 50)

    context.beginPath()
    context.rect(10, 70, 80, 50)
    context.closePath()
    context.fill()

    context.strokeRect(100, 10, 80, 50)

    context.beginPath()
    context.rect(100, 70, 80, 50)
    context.closePath()
    context.stroke()
}

function font_stroke(context) {
    context.font = '18pt Arial'
    context.strokeText('The quick brown fox ', 10, 50)
    context.strokeText('jumps over the lazy dog', 10, 100)
}

function gradients(context) {
    const gradients = [
        context.createLinearGradient(0, 0, 100, 50),
        context.createRadialGradient(50, 80, 10, 50, 100, 60),
        context.createConicGradient(1, 150, 30), // experimental
    ]
    gradients.forEach(element => {
        element.addColorStop(0, "red")
        element.addColorStop(0.5, "green")
        element.addColorStop(1, "violet")
    });
    context.fillStyle = gradients[0]
    context.fillRect(10, 10, 80, 50)
    context.fillStyle = gradients[1]
    context.fillRect(10, 70, 80, 50)
    context.fillStyle = gradients[2]
    context.fillRect(100, 10, 80, 50)
}

function shadow(context) {
    context.shadowColor = 'red'
    context.shadowBlur = 2
    context.shadowOffsetX = 5
    context.shadowOffsetY = 5

    context.fillStyle = 'blue'
    context.fillRect(50, 30, 150, 90)
}

// gradient/pattern

const fp_methods = [
    fpjs3,
    fpjs3_no_text,
    intersect,
    rect,
    rect_4lines,
    //rectCompare,
    bezier,
    diagonalLine,
    straightLine,
    px1line,
    arc,
    bezier_complex,
    font,
    emoji,
    //font_stroke,
    triangle,
    //gradients,
    //shadow,
]



// detect GPU
const webgl_canvas = document.createElement('canvas')
const webgl_context = webgl_canvas.getContext('webgl')
//const vendor = webgl_context.getParameter(webgl_context.VENDOR)
//const renderer = webgl_context.getParameter(webgl_context.RENDERER)
const debug_info = webgl_context.getExtension('WEBGL_debug_renderer_info')
const unmasked_vendor = debug_info ? webgl_context.getParameter(debug_info.UNMASKED_VENDOR_WEBGL) : null
const unmasked_renderer = debug_info ? webgl_context.getParameter(debug_info.UNMASKED_RENDERER_WEBGL) : null



/* draw */
const floatingPointTesting = false
// probe floating-point inconsistencies
function floatingPointMods(context) {
    context.lineWidth = 0.9
    context.lineCap = 'round'
    context.lineJoin = 'miter'
    context.miterLimit = 9.3
    context.setLineDash([5.1, 15.2, 4.9])
    context.lineDashOffset = 1.13
    context.fillStyle = 'rgb(31,32,33)'
    context.strokeStyle = 'rgb(160,66,61)'
    context.shadowColor = 'rgb(200,205,211)'
    context.shadowBlur = 4.3
    context.shadowOffsetX = 3.3
    context.shadowOffsetY = 4.1
    context.setTransform(0.9, 0.2, 0.3, 1.1, 2.1, 4.4)
    context.globalAlpha = 0.9
    context.globalCompositeOperation = 'xor'
}

fp_methods.forEach(function (fp_method) {
    const function_name = functionName(fp_method)

    // surrounding div containing header, canvas, and hashes
    let div = document.createElement('div')
    div.id = function_name
    div.classList.add('canvas-box')
    document.getElementById('canvas-container').appendChild(div)

    // canvas header
    const title = document.createElement('h4')
    title.innerText = function_name
    div.appendChild(title)

    // actual canvas
    const canvas = document.createElement('canvas')
    canvas.width = 240
    canvas.height = 140
    div.appendChild(canvas)

    const context = canvas.getContext('2d')

    // draw something
    fp_method(context)
    const img = canvas.toDataURL()
    const pixels = context.getImageData(0, 0, canvas.width, canvas.height).data

    // show hash of PNG
    const img_hash = hash(img)
    let png_hash = document.createElement('p')
    png_hash.innerText = 'PNG hash: ' + img_hash
    div.appendChild(png_hash)

    // show hash of canvas pixels
    const pixel_hash = hash(JSON.stringify(pixels))
    let pixels_hash = document.createElement('p')
    pixels_hash.innerText = 'Pixels hash: ' + pixel_hash
    div.appendChild(pixels_hash)

    // set upload info to filename
    const gpu = unmasked_vendor && unmasked_renderer ? unmasked_vendor + ' ' + unmasked_renderer : ''
    let filename = ua.getOS().name + '-' + ua.getOS().version + '_' + ua.getBrowser().name + '-' + ua.getBrowser().version + '_' + gpu + '_'
    if (floatingPointTesting) {
        filename += '_float'
    }
    document.getElementById('upload-info').value = filename

    // make canvas downloadable
     div.onclick = function () {
        const filename = document.getElementById('upload-info').value + '_' + img_hash + '.png'
        download(img, filename, 'image/png')
    }

    // upload results to server
    document.getElementById('bt-upload').addEventListener('click', function () {
        const fd = new FormData()
        fd.append('function', function_name)
        fd.append('filename', document.getElementById('upload-info').value + '_' + img_hash + '.png')
        // fd.append('floating_point_testing', floatingPointTesting)
        // fd.append('extra', document.getElementById('upload-info').value)
        // fd.append('browser', ua.getBrowser().name)
        // fd.append('browser_version', ua.getBrowser().version)
        // fd.append('os', ua.getOS().name)
        // fd.append('os_version', ua.getOS().version)
        // fd.append('hash', img_hash)
        const b64 = img.replace(/^data:.+;base64,/, '');
        fd.append('data', b64)

        const xhr = new XMLHttpRequest() || ActiveXObject('Microsoft.XMLHTTP')
        xhr.open('POST', '/upload.php', true)
        xhr.onload = function (e) {
            const status = document.getElementById('upload-status')
            if (xhr.status == 200) {
                console.log("Upload of %s successful!", function_name)
                if (status.textContent == "") {
                    status.innerHTML = "&#10003;"
                }
            } else {
                console.warn("Upload of %s failed! Status code %i", function_name, xhr.status)
                status.innerHTML = "&times; Status: " + xhr.status
            }
        }
        xhr.send(fd)
    })
})
