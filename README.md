# Canvas Fingerprinting Test Page

This repository contains the code of the test page created alongside my bachelor thesis.

The page is directly accessible [through GitLab Pages](https://mynacol.gitlab.io/ba-fingerprint/).
As GitLab Pages only delivers static content, the upload functionality is broken.


## Implementation

The main file of interest is `canvas.js`. At the top are some helper functions, but soon the different canvas image functions start.

They each get a [CanvasRenderingContext2D](https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D), which can be used in any way.

The setup for the different image functions is at the bottom. There, for any element in the list `fp_methods`, a new HTML canvas is created and the current canvas image function called.
Afterwards, a hash of the PNG and a hash of the actual pixel values are calculated and added.

Finally, the canvas is set up, so that a click on it triggers the download of the PNG with the input box as file name.
Equally, an AJAX request is prepared, so that with a click on the upload button, all PNGs are uploaded, including the name of the canvas image functions and the input field content.
A simple PHP file, `upload.php`, receives this data and saves it on disk.


## Dependencies

This project uses some code from other people. Thank you!
- `download.js` aids in clickable images, so you can easily save them on disk. [From _dandavis_](http://danml.com/download.html).
- `ua-parser.min.js` aids in automatically detecting the browser and operating system to auto-fill the input box. [From _Faisal Salman_](https://github.com/faisalman/ua-parser-js).
- `functionName` in canvas.js allows you to use the name of a function programmatically and in older browsers. [From _user1115652_](https://stackoverflow.com/a/17923727).
- `hash` in canvas.js can hash any string in JavaScript. [From _esmiralha_](https://stackoverflow.com/a/7616484).
