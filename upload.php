<?php

declare(strict_types=1);

# extract form data
$function = $_POST["function"];
$filename = $_POST["filename"];
$img = base64_decode($_POST["data"], TRUE);

# save image
$filename = $_SERVER['DOCUMENT_ROOT'] . "/images/$function/$filename";
if (!is_dir(dirname($filename))) {
   mkdir(dirname($filename), 0755, true);
}
$fp = fopen($filename, 'wb');
fwrite($fp, $img);
fclose($fp);